package jsonproto

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"strings"
	"testing"
	"time"
)

const inputStream = `{
  "type": "status",
  "code": 201,
  "value": "created"
}
{
  "type": "list",
  "items": [
    {
      "name": "telephone",
      "value": 155
    },
    {
      "name": "laptop",
      "value": 10155
    }
  ]
}
{
  "type": "status",
  "code": 404,
  "value": "not found"
}`

var results = [...]string{
	"type=status     datetime=0001-01-01 00:00:00 +0000 UTC, 59 bytes",
	"type=list       datetime=0001-01-01 00:00:00 +0000 UTC, 154 bytes",
	"type=status     datetime=0001-01-01 00:00:00 +0000 UTC, 61 bytes",
}

func TestMessageUnmarshalling(t *testing.T) {
	dec := json.NewDecoder(bytes.NewBufferString(inputStream))

	msg := &message[SampleHeader]{}
	i := 0
	for dec.More() {
		if err := dec.Decode(&msg); err == nil {
			if msg.String() != results[i] {
				t.Errorf("wait for '%s', got '%s'", results[i], msg.String())
			}
		}
		i++
	}
}

const noHeaderData = `
{
	"from": "Deer",
	"to": "Santa"
}`

func TestInvalidHeader(t *testing.T) {
	dec := json.NewDecoder(strings.NewReader(noHeaderData))
	msg := &message[SampleHeader]{}
	if dec.More() {
		err := dec.Decode(&msg)
		if err != ErrInvalidHeader {
			t.Errorf("wait for '%s', got '%s'", ErrInvalidHeader, err)
		}
	}
}

func TestParseReader(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	r := bytes.NewBufferString(inputStream)
	logger := &log.Logger{}

	// Parsing gonna terminated by ctx timeout
	src := StreamParser[SampleHeader](ctx, r, logger)

	i := 0
	for msg := range src {
		if results[i] != msg.String() {
			t.Errorf("wait for '%s', got '%s'", results[i], msg)
		}
		i++
	}
}
