package jsonproto

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"time"
)

// StreamParser runs goroutine that reads input stream, parses messages and sends them into the channel.
// Used by Serve function, so it not intended to be called directly.
func StreamParser[T Header](ctx context.Context, r io.Reader, lg *log.Logger) <-chan Message[T] {
	ch := make(chan Message[T])
	go func() {
		dec := json.NewDecoder(r)

		for {
			if ctx.Err() != nil {
				close(ch)
				return
			}
			if dec.More() {
				msg := new(message[T])
				err := dec.Decode(msg)
				if err != nil {
					if err != ErrInvalidHeader {
						lg.Printf("Parser error: %s", err)
					}
					continue
				}
				ch <- msg
			} else {
				time.Sleep(ParseTimeOut)
			}
		}
	}()
	return ch
}

// StreamWriter runs goroutine that listen to the channel and sends data to output io.Writer stream.
// Used by Serve function, so it not intended to be called directly.
func StreamWriter(ctx context.Context, w io.Writer, lg *log.Logger) chan<- []byte {
	ch := make(chan []byte)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case data := <-ch:
				_, err := w.Write(data)
				if err != nil {
					lg.Printf("response writing err: %s", err)
					return
				}
			}
		}
	}()
	return ch
}

// ServeStream function reads input stream, tries to parse user-defined protocol Header,
// then runs a Handler on parsed Message and writes response to output stream.
// Input and output sterams are seperated for versatility and testability, but in
// most cases may be represented by one io.ReadWriter stream.
func ServeStream[T Header](ctx context.Context, r io.Reader, w io.Writer, hdl Handler, lg *log.Logger) {
	serveCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	src := StreamParser[T](serveCtx, r, lg)
	dst := StreamWriter(serveCtx, w, lg)

	for {
		select {
		case <-ctx.Done():
			cancel()
			return
		case msg, ok := <-src:
			if !ok {
				cancel()
				return
			}
			go func() {
				handlerCtx, cancel := context.WithCancel(ctx)
				defer cancel()

				res, err := hdl.Handle(handlerCtx, msg)
				if err != nil {
					lg.Println(err)
				} else if len(res) > 0 {
					dst <- res
				}
			}()
		}
	}
}
