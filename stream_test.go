package jsonproto

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"testing"
	"time"
)

const inputStream2 = `{
  "from": "Deer",
  "to": "Santa",
  "duration": 5,
  "loud": false
}
{
  "from": "Santa",
  "message": "Hello!"
}
{
  "from": "Deer",
  "to": "Santa",
  "duration": 5,
  "loud": true

}
{
  "from": "Deer",
  "to": "Jack Frost",
  "duration": 5,
  "loud": false
}`

var outputStreamFragments = [...]string{
	`{"message":"Too loud"}`,
	`{"message":"Don't bother me"}`,
	`{"message":"strictly prohibited"}`,
}

type customHeader struct {
	From string `json:"from"`
	To   string `json:"to"`
}

func (ch customHeader) Ok() bool {
	return ch.From != "" && ch.To != ""
}

func (ch customHeader) String() string {
	return fmt.Sprintf("from %s to %s", ch.From, ch.To)
}

func m1(input any) bool {
	if jm, ok := input.(Message[customHeader]); ok {
		if jm.Header().From == "Deer" && jm.Header().To == "Santa" {
			return true
		}
		return false
	}
	panic(ErrInvalidHeader)
}

func m2(input any) bool {
	if jm, ok := input.(Message[customHeader]); ok {
		if jm.Header().From == "Deer" && jm.Header().To != "Santa" {
			return true
		}
		return false
	}
	panic(ErrInvalidHeader)
}

type DeerPhrase struct {
	Duration int  `json:"duration,omitempty"`
	Loud     bool `json:"loud,omitempty"`
}

func p1(ctx context.Context, input any) ([]byte, error) {
	if m, ok := input.(Message[customHeader]); ok {
		phrase := DeerPhrase{}
		if err := json.Unmarshal(m.Body(), &phrase); err != nil {
			return []byte(`{"message":"What?"}`), nil
		}
		if phrase.Loud {
			return []byte(`{"message":"Too loud"}`), nil
		}
		return []byte(`{"message":"Don't bother me"}`), nil
	}
	return nil, fmt.Errorf("Not a message")
}

func p2(ctx context.Context, input any) ([]byte, error) {
	return []byte(`{"message":"strictly prohibited"}`), nil
}

func TestServeStream(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	r := bytes.NewBufferString(inputStream2)
	w := &bytes.Buffer{}

	logger := log.New(os.Stdout, "", 1)

	router := NewRouter()
	router.Add(m1, p1)
	router.Add(m2, p2)

	ServeStream[customHeader](ctx, r, w, router, logger)

	for _, fragment := range outputStreamFragments {
		if !strings.Contains(w.String(), fragment) {
			t.Errorf("wait '%s' in responses, got '%s'", fragment, w.String())
		}
	}
}
