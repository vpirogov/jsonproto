package jsonproto

import "context"

// MatchFn used in Router to determine the need of processing a Message[T] with
// correspondent ProcessFn. Implementation should extract Message[user-defined-Header]
// from the argument and use message Header to make decision. Extraction should not fail
// because user-defined-Header already has been used during unmarshalling Message.
type MatchFn = func(any) bool

// ProcessFn generates response on Message as marshalled JSON byte slice.
// It can extract concrete message from Message[T] body. If response should not be sent,
// ProcessFn must return empty or nil byte slice.
type ProcessFn = func(context.Context, any) ([]byte, error)

// MessageHook do things on messages.
// Should perform type Message[T] assertion on second argument.
type MessageHook = func(context.Context, any)

// ResultHook do things on handler result byte slice
type ResultHook = func(context.Context, []byte)

// Router is a Handler implementation with multiple Handlers.
type Router struct {
	matchers       []MatchFn
	processors     []ProcessFn
	BeforeMatching MessageHook
	OnMatch        MessageHook
	OnNotMatch     MessageHook
	OnProcess      ResultHook
}

// NewRouter creates new empty Router
func NewRouter() *Router {
	m := make([]MatchFn, 0)
	p := make([]ProcessFn, 0)
	return &Router{matchers: m, processors: p}
}

// Add method registers a MatchFn + ProcessFn pair as a Message[T] handlers.
// Router runs that functions in order of addition. Once MatchFn returns true,
// the routing process is finished and paired ProcessFn is called on Message[T].
func (r *Router) Add(mf MatchFn, pf ProcessFn) {
	r.matchers = append(r.matchers, mf)
	r.processors = append(r.processors, pf)
}

// Handle implements the Handler interface
func (r *Router) Handle(parent context.Context, input any) ([]byte, error) {
	ctx, cancel := context.WithCancel(parent)
	defer cancel()

	if r.BeforeMatching != nil {
		r.BeforeMatching(ctx, input)
	}

	for i, m := range r.matchers {
		if m(input) {
			if r.OnMatch != nil {
				r.OnMatch(ctx, input)
			}

			res, err := r.processors[i](ctx, input)
			if err != nil {
				if r.OnProcess != nil {
					r.OnProcess(ctx, res)
				}
			}
			return res, err
		}
	}

	if r.OnNotMatch != nil {
		r.OnNotMatch(ctx, input)
	}
	return nil, nil
}
