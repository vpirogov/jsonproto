package jsonproto

import (
	"fmt"
	"time"
)

// SampleHeader is an example of Header implementation
type SampleHeader struct {
	Typ      string    `json:"type"`
	DateTime time.Time `json:"datetime,omitempty"`
}

// Ok impements Header interface
func (h SampleHeader) Ok() bool {
	return h.Typ != ""
}

// String implements Header interface
func (h SampleHeader) String() string {
	return fmt.Sprintf("type=%-10s datetime=%v", h.Typ, h.DateTime)
}
