package jsonproto

import (
	"context"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// GorillaParser TODO runs goroutine that reads input stream, parses messages and sends them into the channel.
// Used by Serve function, so it not intended to be called directly.
func GorillaParser[T Header](ctx context.Context, conn *websocket.Conn, l *log.Logger) <-chan Message[T] {
	ch := make(chan Message[T])
	go func() {
		for {
			if ctx.Err() != nil {
				if l != nil {
					l.Println("parser canceled")
				}
				close(ch)
				return
			}
			msg := new(message[T])
			if err := conn.ReadJSON(&msg); err != nil {
				if websocket.IsUnexpectedCloseError(err, websocket.CloseNormalClosure) {
					if l != nil {
						l.Printf("parser terminated: %s\n", err)
					}
					close(ch)
					return
				}
				if websocket.IsCloseError(err, websocket.CloseNormalClosure) {
					if l != nil {
						l.Println("parser terminated")
					}
					close(ch)
					return
				}
				if l != nil {
					l.Printf("unmarshalling failed: %s\n", err)
					continue
				}
			}
			if l != nil {
				l.Printf("parser got mesage: %s\n", msg)
			}
			ch <- msg
		}
	}()
	return ch
}

// GorillaWriter TODO runs goroutine that listen to the channel and sends data to output io.Writer stream.
// Used by Serve function, so it not intended to be called directly.
func GorillaWriter(ctx context.Context, conn *websocket.Conn, l *log.Logger) chan<- []byte {
	ch := make(chan []byte)
	go func() {
		for {
			select {
			case <-ctx.Done():
				if l != nil {
					l.Println("writer canceled")
				}
				conn.Close()
				return
			case data := <-ch:
				err := conn.WriteMessage(websocket.TextMessage, data)
				if l != nil {
					l.Printf("writer writed response length %d\n", len(data))
				}
				if err != nil {
					if l != nil {
						l.Printf("writer terminated: %s\n", err)
					}
					return
				}
			}
		}
	}()
	return ch
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// NewHTTPHandler TODO function reads input stream, tries to parse user-defined protocol Header,
// then runs a Handler on parsed Message and writes response to output stream.
// Input and output sterams are seperated for versatility and testability, but in
// most cases may be represented by one io.ReadWriter stream.
func NewHTTPHandler[T Header](ctx context.Context, hdl Handler, l *log.Logger) (func(w http.ResponseWriter, r *http.Request), *websocket.Conn) {
	var conn *websocket.Conn

	handler := func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}
		ctx, cancel := context.WithCancel(r.Context())
		defer cancel()

		src := GorillaParser[T](ctx, conn, l)
		dst := GorillaWriter(ctx, conn, l)

		for {
			select {
			case <-ctx.Done():
				if l != nil {
					l.Println("processing cancelled")
				}
				cancel()
				return
			case msg, ok := <-src:
				if !ok {
					if l != nil {
						l.Println("channel closed, processing terminated")
					}
					cancel()
					return
				}
				if l != nil {
					l.Printf("got %s\n", msg)
				}
				go func() {
					res, err := hdl.Handle(ctx, msg)
					if err != nil {
						if l != nil {
							l.Printf("handler error %s\n", err)
						}
					} else if len(res) > 0 {
						dst <- res
						if l != nil {
							l.Printf("message %s processed\n", msg)
						}
					}
				}()
			}
		}
	}
	return handler, conn
}
