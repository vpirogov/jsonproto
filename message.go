package jsonproto

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

var (
	ErrInvalidHeader = errors.New("cannot parse header")
	ErrNotFound      = errors.New("route not found")

	ParseTimeOut = 1 * time.Second
)

// Header interface is used for checking incomming JSON message for correctness.
// Users should define its own protocol header structure that should conforms Header
// interface.
type Header interface {
	// Ok is used during unmarshalling of Message[T]. If Ok returns false,
	// unmarshalling fails and further processing stops.
	Ok() bool
	fmt.Stringer
}

// Message[T] contains user-defined protocol Header and full message body.
// A Header is used for initial validation and handler selection (routing).
// A handler can unmarshal concrete message from the body.
type Message[T Header] interface {
	// Header method returns the user-defined message header,
	// which should be used for routing to appropriate handler.
	Header() T
	// Body returns byte slice that contains full raw message
	// (including Header). It should be used for unmarshalling
	// specific message structure inside a handler.
	Body() []byte
	String() string
}

// Handler interface is used in Serve* functions
type Handler interface {
	Handle(context.Context, any) ([]byte, error)
}

type message[T Header] struct {
	header T
	body   []byte
}

func (m *message[T]) UnmarshalJSON(b []byte) error {
	if err := json.Unmarshal(b, &m.header); err != nil {
		return err
	}
	if !m.header.Ok() {
		m.flush()
		return ErrInvalidHeader
	}

	body := make([]byte, len(b))
	copy(body, b)
	m.body = body

	return nil
}

func (m message[T]) String() string {
	return fmt.Sprintf("%s, %d bytes", m.header, len(m.body))
}

func (m *message[T]) flush() {
	m.header = *new(T)
	m.body = nil
}

func (m message[T]) Header() T {
	return m.header
}

func (m message[T]) Body() []byte {
	return m.body
}
